#Importing Required Libraries
import streamlit as st
import numpy as np
import tensorflow 
from tensorflow import keras
from keras.models import Sequential
from keras.layers import Flatten, Dense, Conv2D, MaxPool2D, ZeroPadding2D, Dropout

from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras import optimizers

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import tight_layout
import random
import pandas as pd

from tensorflow.keras.preprocessing.image import load_img, img_to_array
import cv2                        # This allows us to reproduce the results from our script
from keras.optimizers import Adam, SGD
from keras.utils import np_utils        
from keras.preprocessing import image

import pickle
import os
import urllib

@st.cache
def predict(test_image_file):
    model = keras.models.load_model("malaria_classification")
    #Getting Test image to usable format
    test_image = image.load_img(test_image_file, target_size=(64, 64))
    test_image = image.img_to_array(test_image)
    test_image = np.expand_dims(test_image, axis=0)
    
    #Predicting
    prediction = model.predict(test_image)
    
    return prediction
    

def detect(path):
    rad = 11
    image = cv2.imread(path)
    orig = image.copy()
    red_contrast = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    gray = cv2.cvtColor(red_contrast, cv2.COLOR_BGR2GRAY)
    # apply a Gaussian blur to the image then find the brightest region
    gray = cv2.GaussianBlur(gray, (rad, rad), 0)
    (minVal, maxVal, minLoc, maxLoc) = cv2.minMaxLoc(gray)
    image = orig.copy()
    cv2.circle(image, maxLoc, rad, (255, 0, 0), 2)
    return image

def file_selector(folder_path='.'):
    filenames = os.listdir(folder_path)
    selected_filename = st.selectbox('Upload an Image', filenames)
    return os.path.join(folder_path, selected_filename)

with st.beta_container():
    try:
        st.title('MALARIA TEST')
        
        #Accepting Image from Streamlit
        
        test_image = file_selector()
        st.write('You selected `%s`' % test_image)
        if(test_image[-4:].lower() != '.png' and test_image[-4:].lower() != '.jpg' and test_image[-5:].lower() != '.jpeg'):
            st.write("Please select a jpg,png or jpeg file.")
        else:
            prediction = predict(test_image)
            if prediction[0][0] == 0:
                st.write("Uh oh, this cell is infected with Malaria.")
                
                image = detect(test_image)
                st.text("Detected Image")
                st.image(image)
                
            else:
                st.write("Safe and Sound! This cell is uninfected by Malaria.")
                image = cv2.imread(test_image)
                st.text("Original Image")
                st.image(image)

        
    except urllib.error.URLError as e:
        st.error(
            """
            *This demo requires internet access.*

            Connection error: %s
        """
            % e.reason
        )